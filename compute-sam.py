#!/usr/bin/python
# python2, needs http://bitbucket.org/dradice/scidata
#
# Compute specific angular momentum for 2D HDF5 Cactus output.
#
# 2016, SvenK.

from spacetime_loader import * #SpacetimeLoader
from pylab import *
from mpl_toolkits.axes_grid1 import make_axes_locatable
import sys
ion()

# load all the fields
need_load = True
if(need_load):
	db = SpacetimeLoader()
	db.load_cached("alp, betax, betay, betaz")
	db.load_cached("w_lorentz, vel[0], vel[1], vel[2]")
	db.load_cached("gxx, gxy, gxz, gxy, gyy, gyz, gzz")
	db.load_cached("rho")
	db2 = SpacetimeLoader()
	db2.load("rho")
	db2.load_grid()
	##db.load4d(skip=1) # was 10

# clean broken data
cleaning = lambda f: f[::2]
db.clean4d(cleaning)
#db2.clean4d(cleaning)


globals().update(db.data)
globals().update(db2.data)

def compute_specific_angular_momentum():
	# this is the algorithm from Whisky_SpecificAngularMomentum, cf.
	# https://bitbucket.org/whiskydevs/thcsupport/src/c005080e99e6ff99db817b7a8d1687bc047ccf66/Whisky_Analysis/src/Whisky_SpecificAngularMomentum.cc?at=newthorn-whisky-analysis&fileviewer=file-view-default

	# Description text there (2009, David Link):
	#   Computes the specific angular momentum distribution and the
	#   covariant time component of the four-velocity.
	#   The following definition is taken for the specific angular momentum:
	#          l = - u_phi / u_t
	#   where the indices stand below and denote covariant components.
	#   A different definition for specific angular momentum is
	#          l = h * u_phi
	#   where again the index stands below. For a comparison, the second
	#   definition can be recovered during post-processing by calculating
	#   u_phi from l and u_t first.

	# Calculates the angular velocity omega in Cartesian coordinates.
	omegax = alp*velx - betax
	omegay = alp*vely - betay

	# Transforms the angular velocity to cylindrical coordinates.
	angular_velocity = (x*omegay - y*omegax) / (x*x + y*y)

	# Calculates the covariant components of the shift vector.
	betax_low = gxx*betax + gxy*betay + gxz*betaz
	betay_low = gxy*betax + gyy*betay + gyz*betaz
	betaz_low = gxz*betax + gyz*betay + gzz*betaz

	# Calculates the necessary components of the metric tensor
	# in cylindrical coordinates.
	gtphi   = x*betay - y*betax
	gphiphi = y*y*gxx + x*x*gyy - 2*x*y*gxy
	gtt     = -alp*alp + betax*betax_low \
		  + betay*betay_low + betaz*betaz_low

	# Calculates the covariant t component of the four-velocity.
	ut_low = w_lorentz * ( gtt / alp \
		 + betax_low * (velx - betax/alp) \
		 + betay_low * (vely - betay/alp) \
		 + betaz_low * (velz - betaz/alp) )

	# Calculates the specific angular momentum.
	specific_angular_momentum = \
	    -(gtphi + gphiphi*angular_velocity) \
	   / (gtt   + gtphi  *angular_velocity)

	return (angular_velocity, specific_angular_momentum)

def universe(data, t=None, ax=None, **imshow_kwargs):
	"""
	Plot spacetime diagram, as in rug's "universe"
	function.
	"""
	# imshow extend (left, right, bottom, top)
	extent = (0, amax(x), 0, int(amax(times)))
	imshow(data, interpolation='Nearest', aspect='auto',
		origin='lower', extent=extent, **imshow_kwargs)
	if t: title(t)
	if ax:
		divider = make_axes_locatable(ax)
		cax = divider.append_axes("right", size="5%", pad=0.05)
		colorbar(cax=cax)
	else: 	colorbar()

Omega, l = compute_specific_angular_momentum()

ax1 = subplot(131)
xlabel("x [M]"); ylabel("time [M]")
universe(log10(rho[:,0]), "log(rho)", ax1)

ax2 = subplot(132)
ax2.yaxis.set_ticklabels([])
universe(l[:,0], "Specific angular momentum", ax2, vmin=0, vmax=10)

ax3 = subplot(133)
ax3.yaxis.set_ticklabels([])
universe(Omega[:,0], "Angular velocity Omega", ax3)


#figure()
#for t in range(90,100): plot(Omega[t,0,0:35])

