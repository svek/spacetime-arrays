#!/usr/bin/python
#
# Python2, needs Pylab (numpy) and scidata
# http://bitbucket.org/dradice/scidata
#

#
# 2016, SvenK.

from pylab import *
from scidata.carpet import hdf5 as carpetfile
from re import split, sub
from os import path
from sys import exit


class SpacetimeLoader:
	"""
	SpacetimeLoader is a kind of "frontend library" for scidata
	which allows convenient	working with fields coming out of an Einstein
	Toolkit simulation on a numpy basis in (I)Python, ie.

	> omegax = alp*velx - betax
	> gtphi  = x*betay - y*betax

	here all fields are the discretized 4D (time,space) cartesian grids
	for a given mesh refinement level. The mechanism to allow clean
	variable names is called "globalization" and just copies the
	content of the dict self.data to the globals().

	As input, Carpet HDF5 files are taken via scidata. As scidata has
	a query interface, conversion to a single numpy array takes quite
	some time for long simulations (with lots of different time steps).

	In order to speed up program starts, the class can dump and 
	read in again the numpy arrays to files.
	"""
	def getfname(self, f, fileformat='h5'):
		"""
		Convenient way to alias field names: Call load("rho")
		and getfname makes sure this translates to rho.xy.h5.
		See register() for a more generic way, ie.
		> l.register("rho", "/foo/bar/rho.xy.h5")
		> l.load("rho")
		"""
		return path.join(self.datadir, "%s.xy.%s" % (f, fileformat))

	def populate(self,k,v):
		k = self.canonicalname(k)
		self.data[k] = v
		# announce variable in python global namespace
		if self.globalize:
			return globals().update({ k:v })

	def __init__(self, datadir='./', globalize=True):
		self.datadir = datadir
		self.files = {}
		self.fields = []
		self.field_keys = [] # without x,y,iter
		self.data = {}
		self.globalize = globalize
		self.max_iter = None

	def register(self, fieldname, filename):
		"""
		This method is the interface to scidata. Here, and instance
		of carpetfile is created.
		"""
		self.files[fieldname] = carpetfile.dataset(filename)

	def canonicalname(self, fieldname):
		for k,s in {'[0]':'x', '[1]':'y', '[2]':'z'}.iteritems():
			fieldname = fieldname.replace(k, s)
		return fieldname

	def load(self, fieldlist):
		"""
		Load the t=0 to global variables. Eg.
		> l = Loader(datadir="./data")
		> l.load("rho, p")
		> foo = rho*p	
		"""
		# load from list or comma or whitespace seperated string
		fields = split(r',\s*|\s+', fieldlist) if (type(fieldlist) is str) else fieldlist
		self.fields += fields

		# substitute like vel[0] to velx
		keys = [ self.canonicalname(f) for f in fields ]
		self.field_keys += keys
	
		# load fields
		for fieldname, key in zip(fields,keys):
			h5fname = self.getfname(fieldname)
			print "Loading Carpet file %s ..." % h5fname
			self.register(fieldname, h5fname)

		return self.select()

	def load_cached(self, fieldlist):
		"""
		This function will use a cached *.npy file, if present. The stored file
		in numpy format basically acts as a cache.
		"""
		# load from list or comma or whitespace seperated string
		fields = split(r',\s*|\s+', fieldlist) if (type(fieldlist) is str) else fieldlist
		self.fields += fields

		# substitute like vel[0] to velx
		keys = [ self.canonicalname(f) for f in fields ]
		self.field_keys += keys

		# load fields
		for fieldname, key in zip(fields,keys):
			cachefname = self.getfname(fieldname, fileformat='npy')
			print "Loading cached %s ..." % cachefname
			self.populate(fieldname, load(cachefname))

		# NO call to self.select().

	def select(self,it=None,rl=0,populate=False):
		"""
		Load a timestep (to the globals/or return).
		Default for it is the first iteration, iteration[0]. Most likely this is 0.
		"""
		if it == None:
			my_grid = self.load_grid(rl=rl, populate=False)
			it = my_grid['iters'][0]
		ret={}
		for fieldname, key in zip(self.fields, self.field_keys):
			dfile = self.files[fieldname]
			# grid is instance of scidata.carpet.grid.grid
			grid = dfile.get_grid(iteration=it)
			# grid.levels are instances of scidata.carpet.grid.level,
			# each level as a unique numerical id what we
			# identify with the refinement level id.
			levelids = [level.id for level in  grid.levels]
			if not rl in levelids:
				# skip this timestep
				print "Skipping it=%d because rf=%d is not contained" % (it, rl)
				continue
			# data is of same list shape as levelids
			data = dfile.get_grid_data(grid, iteration=it)
			# identify data based on their reflevel id
			dataForReflevel = dict(zip(levelids, data))

			ret[key] = dataForReflevel[rl]
			if populate:
				self.populate(key, data[rl])

		# you can also use the return value
		return ret

	def load_grid(self, rl=0, populate=True):
		"""
		Load generic grid information, based on the first file (ie. first
		entry in self.files). We assume that the grid is the same for all
		fields.
		"""
		print "Loading grid information ..."
		if(not len(self.files)):
			raise ValueError("Call load() first to load some fields")
		dfile = self.files.values()[0]
		grid = dfile.get_grid()
		rlevel = grid.levels[rl]
		x, y = grid.mesh()[rl]
		iters = array(dfile.iterations)
		times = [dfile.get_time(i) for i in iters]
		if self.max_iter:
			iters = iters[:self.max_iter]
			times = times[:self.max_iter]

		if populate:
			self.populate('iters',iters)
			self.populate('times', times)
			self.populate('x',x)
			self.populate('y',y)

		return { 'iters': iters, 'times': times, 'x': x, 'y': y }

	def load4d(self, rl=0, skip=1):
		"""
		Load the full 4d arrays. In contrast to a parameterless call to
		select(), after calling this method (which takes quite some time),
		the self.data are really 4D.

		Make sure you call load_grid() before because access to 'iters'
		and 'x' is needed.
		"""
		print "Loading 4d of all data."

		iters = self.data['iters'][::skip]
		timeshape = (len(iters),) + self.data['x'].shape
		data4d = {k: ndarray(timeshape) for k in self.field_keys }

		# parallalelize here...
		for i,it in enumerate(iters):
			print "Timestep %d from %d: %d" % (i, len(iters), it)
			for k, v in self.select(it=it, populate=False).iteritems():
				data4d[k][i] = v

		print "Finished"
		self.data = data4d

		if self.globalize:
			for k,v in self.data.iteritems():
				globals().update({ k:v })
		return self.data

	def clean4d(self, rule=lambda a: a[::2]):
		"""
		Cleans up timesteps where there are no data, as for instance
		one has not correctly dumped them out and when reading them in,
		they have empty values. This is done via a callback function.
		"""
		for k in self.data.keys():
			self.data[k] = rule(self.data[k])
		# do not even globalize as... this does not work anyway
		# from the module to the globals.
		return self.data

