# spacetime-arrays for Cactus and numpy #

This is a small "glue code" to allow to write code like

```
foo = rho * press / (x * gxx + y * gyy**2)
```

in (I)Python with numpy arrays holding 2D, 3D data with times, each. That is, it closes the gap between the query-based [Cactus](http://cactuscode.org/) [hdf5](https://www.hdfgroup.org/HDF5/) interface of [scidata](https://bitbucket.org/dradice/scidata) and the [n-dimensional array](http://docs.scipy.org/doc/numpy/reference/arrays.ndarray.html) of scipy.

The repository holds an example file where the specific angular momentum and the angular velocity (both frame dependent quantities) are computed on the fly for a spacetime diagram, that is, a 2D diagram `y(t)`.

A special feature of this code is the caching of the produced data structures as `npy` files. This allows a superfast restart (<1sec) after the data had been transfered to the favoured numpy format (~1min).

## How to use 

* Clone the repository on your disk
* Do the same with [scidata](https://bitbucket.org/dradice/scidata)
* Try one of the shellscripts or python scripts