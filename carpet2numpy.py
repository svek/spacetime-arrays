#!/usr/bin/env python
#
# Python2 with pylab and scidata dependency, cf. spacetime_loader.

import sys
from spacetime_loader import SpacetimeLoader
import numpy as np

load_grid = 'grid'

if len(sys.argv) != 2:
	print "Error: Need exactly one argument."
	print "Usage: %s <fieldname>" % sys.argv[0]
	print "Tries to slurp something like $PWD/<fieldname>.xy.h5"
	print "and writes to $PWD/<fieldname>.xy.np"
	print "IF <fieldname> == '%s', then the grid (x,y,z) is stored" % load_grid
	sys.exit(-1)

fieldname = sys.argv[1]
db = SpacetimeLoader(globalize=False)
outfilename = db.getfname(fieldname, fileformat='npy')
canonicalname = db.canonicalname(fieldname)

if fieldname == load_grid:
	print "Grid loading currently not supported. Just load an arbitrary field"
	print "in the user code and call db.load_grid()"
	sys.exit(-2)

db.load(fieldname)
print "Slurping field to numpy, saving to %s afterwards." % outfilename
db.load_grid()
fields = db.load4d()
field = fields[canonicalname]

np.save(outfilename, field)

print "Done writing out %s" % outfilename
