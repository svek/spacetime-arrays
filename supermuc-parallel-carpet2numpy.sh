#!/bin/sh

. /etc/profile
. /etc/profile.d/modules.sh

module load python/2.7_anaconda
export PYTHONPATH="$HOME/cactus-analysis/scivis/scidata"

~/cactus-analysis/spacetime-arrays/parallel-carpet2numpy.sh $@
