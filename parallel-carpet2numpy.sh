#!/bin/bash

SCRIPTHOME="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

FIELDS="alp betax betay betaz w_lorentz vel[0] vel[1] vel[2] gxx gxy gxz gyy gyz gzz rho"

echo "At $(hostname), in simulation directory $PWD, parallel compute the fields $FIELDS:"

parallel --linebuffer $SCRIPTHOME/carpet2numpy.py '{}' ::: $FIELDS

echo Done
